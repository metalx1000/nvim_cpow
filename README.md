# nvim_cpow

Copyright Kris Occhipinti 2023-04-15

(https://filmsbykris.com)

License GPLv3

~~~bash
#requirements 
sudo apt install git wget gcc npm fzf tmux

#install neovim
sudo apt install neovim

#or
wget "https://github.com/neovim/neovim/releases/download/v0.9.5/nvim-linux64.tar.gz" -O /tmp/nvim-linux64.tar.gz
sudo tar xzvf /tmp/nvim-linux64.tar.gz -C /usr/share
sudo ln -s "/usr/share/nvim-linux64/bin/nvim" /usr/local/bin/nvim

#Download configs
wget "https://gitlab.com/metalx1000/nvim_cpow/-/archive/master/nvim_cpow-master.tar.gz"
tar xvf nvim_cpow-master.tar.gz

#Backup Current Config
tar -czvf "$HOME/Downloads/nvim_$(date +%Y-%m-%d).tar.gz" ~/.cache/nvim ~/.config/nvim ~/.local/share/nvim

#Remove Current Settup
rm ~/.cache/nvim -fr
rm ~/.config/nvim -fr
rm ~/.local/share/nvim -fr

#Load Configs
mkdir -p $HOME/.config/nvim
cp -vr nvim_cpow-master/config/* $HOME/.config/nvim/

sudo mkdir -p /usr/share/vim/fbk
sudo cp -vr nvim_cpow-master/templates /usr/share/vim/fbk/

#set as replacement for vim
sudo ln -s "/usr/share/nvim-linux64/bin/nvim" /usr/local/bin/vim
~~~


