local function map(m, k, v)
  vim.keymap.set(m, k, v, { silent = true })
end

local o = vim.opt
--fix indenting with F6
local function indent()
  vim.g.tabstop = 2
  vim.g.shiftwidth = 2
  local line = vim.api.nvim_win_get_cursor(0)
  vim.cmd[[ 
  normal ggV999j9<gg=G
  :%s/\s\+$//e
  ]]
  vim.api.nvim_win_set_cursor(0,line)
end
map("n","<F6>", indent)


local function indentphp()
  vim.g.tabstop = 2
  vim.g.shiftwidth = 2
  local line = vim.api.nvim_win_get_cursor(0)
  vim.cmd[[ 
  set filetype=php
  normal gg=G
  set filetype=html
  normal gg=G
  ]]
  vim.api.nvim_win_set_cursor(0,line)
end
map("n","<F7>", indentphp)

--remember the last line you where on in a while at open
vim.cmd[[
if has("autocmd")
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif
]]

vim.cmd[[ au BufEnter,BufNew *.php :set filetype=html ]]

--quit commands
vim.cmd[[
command! Wq :wq
command! W :w
command! Q :q
:cmap Q! q!
:cmap q1 q!
iab Wq <ESC>i<DEL><ESC>:wq<CR>
iab wq <ESC>i<DEL><ESC>:wq<CR>
]]

--JavaScript Abbreviations
vim.cmd[[
iabbrev <buffer> func-- function (){<CR>}<ESC>kf(i<BS>
]]

--HTML Abbreviations
vim.cmd[[

iabbrev <buffer> div-- <div class="" id=""> </div><ESC>F<i<BS>
iabbrev <buffer> form-- <form action="post.php" method="post"></form><ESC>F<i<BS>
iabbrev <buffer> input-- <input type="text" id="" class="" name="" placeholder="" />
iabbrev <buffer> button-- <button class="" id=""></button><ESC>F<i
iabbrev <buffer> img-- <img src="" class="" id="" /><ESC>3F=la
iabbrev <buffer> script-- <script src=""></script><ESC>F"i
iabbrev <buffer> qs-- document.querySelector("");<ESC>F"i
iabbrev <buffer> qsall-- document.querySelectorAll("");<ESC>F"i

" Wrap visual selection in an HTML tag.
vmap <Leader>w <Esc>:call VisualHTMLTagWrap()<CR>
function! VisualHTMLTagWrap()
  let tag = input("Tag to wrap block: ")
  if len(tag) > 0
    normal `>
    if &selection == 'exclusive'
      exe "normal i</".tag.">"
    else
      exe "normal a</".tag.">"
    endif
    normal `<
    exe "normal i<".tag.">"
    normal `<
  endif
endfunction
]]
--run template search
map("i", "<C-t>", "<ESC>:read !/usr/share/vim/fbk/vim_templates.sh<CR>mqgg=G'qzzi")

--type file name
map("i", "<C-F>", "<C-X><C-F>") 

--Spell Check
map("n", "<F9>", ":setlocal spell! spelllang=en_us<CR>")

--load templates
map("n", "<leader>html", ":-1read /usr/share/vim/fbk/templates/html/index.html<CR>3j2f<i")
map("n", "<leader>bash", ":-1read /usr/share/vim/fbk/templates/sh/bash_header.sh<CR>Gi")
map("n", "<leader>php", ":-1read /usr/share/vim/fbk/templates/php/blank<CR>Gi")
--load default templates on new files
vim.cmd[[autocmd BufNewFile  *.html 0r /usr/share/vim/fbk/templates/html/index.html]]
vim.cmd[[autocmd BufNewFile  *.sh 0r /usr/share/vim/fbk/templates/sh/bash_header.sh]]
vim.cmd[[autocmd BufNewFile  *.php 0r /usr/share/vim/fbk/templates/php/blank.php]]

--show relative line number
o.relativenumber = true
vim.cmd[[set mouse-=a]]
--vim.cmd[[colorscheme tokyonight]]


--local x = require('telescope.builtin').find_files(require('telescope.actions.state').get_selected_entry())
--print(x)
o.expandtab = true
o.tabstop=2
o.shiftwidth=2
o.hlsearch = false 
o.incsearch = true
o.scrolloff = 10

--move selected lines up and down when in Visual Mode
map("v","J",":m '>+1<CR>gv=gv")
map("v","K",":m '<-2<CR>gv=gv")

--keep cursor centered while searching
map("n","n","nzzzv")
map("n","N","Nzzzv")

--yank/copy into system clipboard
map("v", "<leader>y", "\"+y")
map("n", "<leader>y", "\"+y")

--replace every instance of current word
map("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

--make current file executable
map("n", "<leader>x", "<cmd>!chmod +x %<CR>")

map('n', '<leader>ff', ":lua require('telescope.builtin').find_files()<CR>")

--swicth buffers using > and <
map('n', '>', ':bnext<CR>')
map('n', '<', ':bprev<CR>')

--paste without losing buffers
map('x', '<leader>p', "\"_dP")


vim.keymap.set('n', '<leader>?', require('telescope.builtin').oldfiles, { desc = '[?] Find recently opened files' })
vim.keymap.set('n', '<leader>ff', require('telescope.builtin').find_files, { desc = '[?] Find recently opened files' })
vim.keymap.set('n', '<leader><space>', require('telescope.builtin').buffers, { desc = '[ ] Find existing buffers' })
