function form2json(event) {
  event.preventDefault();

  let formEntries = new FormData(event.target).entries();
  let json = Object.assign(...Array.from(formEntries, ([x,y]) => ({[x]:y})));
  console.log(json);
  return json;
}

const form = document.querySelector('form');
form.addEventListener('submit', form2json);
