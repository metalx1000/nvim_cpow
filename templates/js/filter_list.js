let filter_input = document.querySelector("#input");
filter_input.focus();
filter_input.addEventListener('input', filter);

function filter(){
  let items = document.querySelectorAll(".items");
  let q = filter_input.value.toUpperCase();
  for( item of items ){
    if(item.innerHTML.toUpperCase().includes(q)){
      item.style.display = "";
    }else{
      item.style.display = "none";
    }
  }
}

